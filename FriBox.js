var port = process.env.PORT || 8080;

var mime = require('mime-types');
var formidable = require('formidable');
var http = require('http');
const fs = require('fs-extra');
var util = require('util');
var path = require('path');

var dataDir = "./data/";

var streznik = http.createServer(function(zahteva, odgovor) {
  if (zahteva.url === '/') {
    posredujOsnovnoStran(odgovor);
  } else if (zahteva.url === '/datoteke') {
    posredujSeznamDatotek(odgovor);
  } else if (zahteva.url.startsWith('/brisi')) {
    izbrisiDatoteko(odgovor, dataDir + zahteva.url.replace("/brisi", ""));
  } else if (zahteva.url.startsWith('/prenesi')) {
    posredujStaticnoVsebino(odgovor, dataDir + zahteva.url.replace("/prenesi", ""), "application/octet-stream");
  } else if (zahteva.url === "/nalozi") {
    naloziDatoteko(zahteva, odgovor);
  } else if(zahteva.url.startsWith('/poglej')) {
    posredujStaticnoVsebino(odgovor, datadir + zahteva.urlreplace("/poglej", ""), "");
  }
  else {
    posredujStaticnoVsebino(odgovor, './public' + zahteva.url, "")
  }
});

streznik.listen(port, function(){
  console.log("Streznik je pognan");
});

function posredujOsnovnoStran(odgovor) {
  posredujStaticnoVsebino(odgovor, './public/fribox.html', "");
}

function posredujStaticnoVsebino(odgovor, absolutnaPotDoDatoteke, mimeType) {
  fs.exists(absolutnaPotDoDatoteke, function(datotekaObstaja) {
    if (datotekaObstaja) {
      fs.readFile(absolutnaPotDoDatoteke, function(napaka, datotekaVsebina) {
        if (napaka) {
          // Posreduj napako
          posreduNapako(odgovor,500);
        } else {
          posredujDatoteko(odgovor, absolutnaPotDoDatoteke, datotekaVsebina, mimeType);
        }
      });
    } else {
      // Posreduj napako
      posredujNapako(odgovor, 404);
    }
  });
}

function posredujDatoteko(odgovor, datotekaPot, datotekaVsebina, mimeType) {
  if (mimeType === "") {
    odgovor.writeHead(200, {'Content-Type': mime.lookup(path.basename(datotekaPot))});
  } else {
    odgovor.writeHead(200, {'Content-Type': mimeType});
  }

  odgovor.end(datotekaVsebina);
}

function posredujSeznamDatotek(odgovor) {
  odgovor.writeHead(200, {'Content-Type': 'application/json'});
  fs.readdir(dataDir, function(napaka, datoteke) {
    if (napaka) {
      // Posreduj napako
      posredujNapako(odgovor, 500);
    } else {
      var rezultat = [];
      for (var i=0; i<datoteke.length; i++) {
        var datoteka = datoteke[i];
        var velikost = fs.statSync(dataDir+datoteka).size;
        rezultat.push({datoteka: datoteka, velikost: velikost});
      }

      odgovor.write(JSON.stringify(rezultat));
      odgovor.end();
    }
  });
}

function izbrisiDatoteko(odgovor, datoteka) {
    odgovor.writeHead(200,{'Content-Type': 'text/plain'});
    fs.unlink(datoteka,function(napaka){
      if(napaka){
        posredujNapako(odgovor,404);
      }
      else{
        odgovor.writeHead(200,"Datoteka izbrisana");
        odgovor.end();
      }
    });
  }

function naloziDatoteko(zahteva, odgovor) {
  var form = new formidable.IncomingForm();

  form.parse(zahteva, function(napaka, polja, datoteke) {
    util.inspect({fields: polja, files: datoteke});
  });

  form.on('end', function(fields, files) {
    var zacasnaPot = this.openedFiles[0].path;
    var datoteka = this.openedFiles[0].name;
    fs.copy(zacasnaPot, dataDir + datoteka, function(napaka) {
      if (napaka) {
        // Posreduj napako
        posredujNapako(odgovor, 500);
      } else {
        posredujOsnovnoStran(odgovor);
      }
    });
  });
}

function posreduNapako(odgovor, tip){
  odgovor.writeHead(tip, {"Content-Type": "text/plain"});
  if(tip==404){
    odgovor.end("Napaka 404: Vira ni mogoče najti.");
  }
  else if (tip==500){
    odgovor.end("Napaka 500: Prišlo je do napake strežnika.");
  }
  else if (tip==409){
    odgovor.end("Napaka 409: ");
  }
}